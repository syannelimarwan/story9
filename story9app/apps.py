from django.apps import AppConfig


class Story9AppConfig(AppConfig):
    name = 'story9app'
