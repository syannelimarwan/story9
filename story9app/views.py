from django.shortcuts import render
from django.contrib.auth import login, authenticate,logout
from django.shortcuts import render,redirect,get_object_or_404
from django.http import HttpResponse

response = {}

def user_login(request):
    response['login_view'] = "active"
    if request.method == "POST":
         username = request.POST['username']
         password = request.POST['password']
         user = authenticate(request, username=username, password=password)
         if user is not None:
            login(request, user)
            request.session['username'] = username
            response['username'] = request.session['username']
            print("login views is running " + request.session['username'])
            return redirect('login')
         else:
            return HttpResponse("Invalid Password or Username. Please try again")
    else:
        return render(request, 'login.html',response)

def user_logout(request):
    request.session.flush()
    logout(request)
    return redirect('login')
